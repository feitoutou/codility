package test;

import org.junit.Assert;
import org.junit.Test;
import self.training.OddConcurrenciesInArray;

public class OddConcurrenciesInArrayTest {

    @Test
    public void testOddConcurrenciesInArray() {
        int [] A = new int[] {9, 3, 9, 3, 9, 7, 9, 7, 11, 7, 11};
        int unpaired = OddConcurrenciesInArray.solution(A);
        Assert.assertEquals(unpaired, 7);
    }
}
