package test;


import org.junit.Assert;
import org.junit.Test;
import self.training.BinaryGap;

public class BinaryGapTest {

    @Test
    public void testSolution() {
        int a = 1;
        int b = 1024 * 1024;
        int aGap = BinaryGap.solution(a);
        int bGap = BinaryGap.solution(b);
        Assert.assertEquals(aGap, 0);
        Assert.assertEquals(bGap, 20);
    }
}
