package test;

import org.junit.Assert;
import org.junit.Test;
import self.training.CyclicRotation;

public class CyclicRotationTest {
    @Test
    public void testSolution() {
        int[] A1 = new int[] {2, 3, 4, 5, 6};
        int K1 = 1;
        int[] AK1 = CyclicRotation.solution(A1, K1);
        int[] A2 = new int[] {-1, 3, 4, 5, 6, 9, 0};
        int K2 = 5;
        int[] AK2 = CyclicRotation.solution(A2, K2);
        Assert.assertArrayEquals(new int[] {6, 2, 3, 4, 5}, AK1);
        Assert.assertArrayEquals(new int[] {4, 5, 6, 9, 0, -1, 3}, AK2);
    }
}
