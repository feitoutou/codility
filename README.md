This project includes my solutions for codility lessons:
https://app.codility.com/programmers/lessons/1-iterations/

It includes IntelliJ IDEA project files, you can simply import to your IDE.
Also, jUnit test cases included.

If you found any bugs or you have better solutionsplease contact me at
alwingao@gmail.com

